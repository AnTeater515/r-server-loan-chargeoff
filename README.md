**NOTE:** Please don't use "Download ZIP" to get this repository, as it will change the line endings in the data files. Use "git clone" to get a local copy of this repository. 

**Live DEMO:** Run DebtChargeOff.pbix

# Debt Charge-off machine learning model
Scenario modelling to mitigate risk in finance.

### Deploying to Azure on SQL Server
I have modeled the steps in the template after a realistic team collaboration on a data science process. Data scientists do the data preparation, model training, and evaluation from their favorite R IDE. DBAs can take care of the deployment using SQL stored procedures with embedded R code. Each of these steps can be executed on a SQL Server client environment such as SQL Server Management Studio. Finally, a Power BI report is used to visualize the deployed results.

![Screenshot](Untitled1.png)


## Deploying to SQL Server On-Premises (with MicrosoftML)
For users who prefer an on-premise solution, the implementation with SQL Server ML Services is a great option that takes advantage of the powerful combination of SQL Server and the R language. A Windows PowerShell script to invoke the SQL scripts that execute the end-to-end modeling process is provided for convenience. 

![Screenshot](Untitled2.png)

